= Timekit Connector
:keywords: timekit, timekit connector, time, user guide.
Release Notes:* link:/release-notes/Timekit-connector-release-notes[Timekit Connector Release Notes]


== Introduction
Timekit Connector is a closed source connector which provides a connection between Mule and third-party software Timekit API. It implements a repertoire of timekit API modules.
The latest Timekit connector is an SDK-based connector developed with * DevKit 3.8.0 *.


=== Prerequisites
This document assumes that you are familiar with the Timekit API.
To use this connector you need the following:

* Create a test account on Timekit.io
* Obtain the API key of the application
* MuleSoft Enterprise License

=== Requirements
Have an account and a token.
link: https://admin.timekit.io/create
See the link: /mule-user-guide/v/3.8/hardware-and-software-requirements [hardware and software requirements].

=== Dependencies
The Timekit connector requires the following dependencies:

[cols="40a,60a",options="header"]
|===
| *Application / Service* |Version
| *Anypoint Studio* |6.5
| *Mule execution time* |3.8.0 and higher (no Mule version 4.x)
| *Timekit* |2.0
| *Java* |JDK 7 and higher
| ===

|===
== How to Install

You can install the connector in Anypoint Studio using the instructions in
link:/getting-started/anypoint-exchange#installing-a-connector-from-anypoint-exchange[Installing a Connector from Anypoint Exchange].

== Using This Connector
list of all the operations:

* Get current app
* Invite resources to app
* Create a resources
* List all resources
* Retrieve a resource
* Update a resource
* Remove a resource
* Query Availability
* Create a booking
* List all bookings
* Retrieve a booking
* Update booking state
* Udate booking meta data
* Create booking in bulk
* Update bookings in bulk
* Delete a booking

== Connector Namespace and Schema
*Namespace:* '+mule-timekit+' +
*Schema Location:* '+http://www.mulesoft.org/schema/mule/timekit/current/mule-timekit.xsd+'

[source, xml,linenums]
----
<mule xmlns:timekit="http://www.mulesoft.org/schema/mule/timekit" xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation"
	xmlns:spring="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-current.xsd
http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/timekit http://www.mulesoft.org/schema/mule/timekit/current/mule-timekit.xsd">

</mule>
----

== Maven Dependency Information
[source,xml,linenums]
----
<dependency>
  <groupId>org.mule.modules</groupId>
  <artifactId>timekit-connector</artifactId>
  <version>1.0.0-SNAPSHOT</version>
</dependency>
----


=== Example Application Setup
* Search for and drag the *HTTP* connector to the Anypoint Studio Canvas
* Search for and drag the *Timekit* connector to the Canvas.

image::_images/timekit-flow.png["Timekit connector initial flow"]

=== Setting up the Connector Global Element
* We enter with double click to the connector to make the configuration

image::_images/timekit-configuration1.png["Empty configuration"]
* is "Basic Settings" we click on the green cross to add a new connection.
in token enter: $ {config.token}
* To obtain the token enter the following link: https://admin.timekit.io/create
- Create an account
- login
- In "API settings" you get the token

image::_images/timekit-configuration2.png["configuration with data"]
* Then we chose for this example the "Get app" operation to obtain information from the user that generated the token.

image::_images/timekit-configuration3.png["final configuration"]

=== Invoking an Operation
* Locate, and drag and drop the *HTTP connector*, and *Timekit connector* onto the Anypoint Studio Canvas.
* Configure the Timekit connector by selecting the *Connector Configuration* you created in the previous section and choosing the operation to invoke.

image::_images/timekit-configuration3.png["finished configuration"]
* The operation to a GET requirement which returns us information of the user that generated the token.

image::_images/timekit-invocation1.png["basic configuration"]
* We execute the project

image::_images/timekit-invocation2.png["basic configuration"]
* We test in a browser typing. localhost: 8081 / test (how do I configure it)

image::_images/timekit-invocation3.png["basic configuration"]


== Resources
*  Detailed version and its functions: release-notes.adoc
* For more information on the Timekit API, visit its link:https://developers.timekit.io/reference[API documentation page].
* link:http://training.mulesoft.com[MuleSoft Training]
* link:https://www.mulesoft.com/webinars[MuleSoft Webinars]
* link:http://blogs.mulesoft.com[MuleSoft Blogs]
* link:http://forums.mulesoft.com[MuleSoft Forums]